import networkx as nx
import matplotlib.pyplot as plt

def recommend_friends(graph, user):
    user_friends = set(graph.neighbors(user))
    recommended_friends = []

    # Perform breadth-first search
    queue = []
    visited = set()

    queue.extend(graph.neighbors(user))
    visited.add(user)

    while queue:
        current_user = queue.pop(0)
        visited.add(current_user)
        current_user_friends = set(graph.neighbors(current_user))

        for friend in current_user_friends:
            if friend not in visited and friend != user:
                recommended_friends.append(friend)
                visited.add(friend)
                queue.append(friend)

    return recommended_friends

def load_graph(file_name):
    graph = nx.Graph()
    with open(file_name, 'r') as file:
        for line in file:
            user1, user2 = line.strip().split(',')
            graph.add_edge(user1, user2)
    return graph

def visualize_graph(graph):
    plt.figure(figsize=(8, 6))
    pos = nx.spring_layout(graph, seed=42)
    nx.draw_networkx(graph, pos, with_labels=True, node_color='lightblue', edge_color='gray')
    plt.title("Social Network Graph")
    plt.axis("off")
    plt.show()

# Load the graph from the file
graph = load_graph("friends.txt")

# Recommend friends for a user
user = "John"
recommended_friends = recommend_friends(graph, user)

# Print the recommended friends
print(f"Recommended friends for {user}: {recommended_friends}")

# Visualize and save the graph as an image
visualize_graph(graph)
plt.savefig("social_network_graph.png")
